<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

require_once '../lib/Bootstrap.php';
use \Phabs\Bootstrap;

class ResourceCollection1 {
	public function initResource1() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function initResource2() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function lazyResource3() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public static function lazyResource4() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
}

class ResourceCollection2 {
	public function initResource5() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function initResource6() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function lazyResource7() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public static function lazyResource8() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
}

echo "==========================================================================\n";
Bootstrap::bootstrap(new ResourceCollection1());
Bootstrap::bootstrap(new ResourceCollection2());
echo "==========================================================================\n";

foreach(Bootstrap::getInstance()->getResources() as $resource) {
	echo $resource->getValue()."\n";
}