<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

require_once '../lib/Bootstrap.php';
use \Phabs\Bootstrap;

function ResourceInitPreload() {
	printf("Resource '%s' has been initialized\n", __METHOD__);
	return sprintf("Returnvalue from resource '%s'", __METHOD__);
}

function ResourceInitLazy() {
	printf("Resource '%s' has been initialized\n", __METHOD__);
	return sprintf("Returnvalue from resource '%s'", __METHOD__);
}

$bootstrap = Bootstrap::getInstance();
$bootstrap->registerResource('lazy_resource', 'ResourceInitLazy');
$bootstrap->registerResource('preloaded_resource', 'ResourceInitPreload', true);

echo "==========================================================================\n";
$bootstrap->init();
echo "==========================================================================\n";

foreach($bootstrap->getResources() as $resource) {
	echo $resource->getValue()."\n";
}
