<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

require_once '../lib/Bootstrap.php';
use \Phabs\Bootstrap;

class ResourceCollection {
	public function initResource1() {
		Bootstrap::requires('resource2');
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function initResource2() {
		Bootstrap::requires(array('resource3', 'resource4'));
		$resource3_value = Bootstrap::getValue('resource3');
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public function lazyResource3() {
		$bootstrap = Bootstrap::getInstance();
		$bootstrap->requires('resource5');
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
	
	public static function lazyResource4() {
		printf("Resource '%s' has been initialized\n", __METHOD__);
		return sprintf("Returnvalue from resource '%s'", __METHOD__);
	}
}

function lazyResource5() {
	printf("Resource '%s' has been initialized\n", __METHOD__);
	return sprintf("Returnvalue from resource '%s'", __METHOD__);	
}

$bootstrap = Bootstrap::getInstance();
$bootstrap->registerObject(new ResourceCollection());
$bootstrap->registerResource('resource5', 'lazyResource5', false);

echo "==========================================================================\n";
$bootstrap->init();
echo "==========================================================================\n";

foreach($bootstrap->getResources() as $resource) {
	echo $resource->getValue()."\n";
}