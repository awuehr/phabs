<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

namespace Phabs;

require_once 'Bootstrap/Exception.php';
require_once 'Bootstrap/Resource.php';

/**
 * Class for bootstrapping resource functions and objects
 */
class Bootstrap {
	/** 
	 * @var string Method prefix for resources to be preloaded
	 */
	protected $preload_prefix = 'init';
	
	/** 
	 * @var string Method prefix for resources to be loaded on first access
	 */
	protected $lazy_prefix = 'lazy';
	
	/** 
	 * @var string Regex pattern for finding resource methods
	 */
	const RESOURCE_METHOD_SEARCH = "/%s(\w+)/iu";
	
	/** 
	 * @var string Regex pattern for getting name and preload type for resource methods
	 */
	const RESOURCE_NAME_SEARCH = "/^(%s)(\w+)$/iu";

	/** 
	 * @var Bootstrap Static instance
	 */
	protected static $instance = null;
	
	/**
	 * @var \ArrayObject Registered resources
	 */
	protected $resources = null;
	
	/**
	 * Constructor for bootstrap class
	 * 
	 * @throws Bootstrap\SingletonViolationException
	 */
	public function __construct() {
		if(self::$instance) {
			throw new Bootstrap\SingletonViolationException('You may not have more than one instance of \Phabs\Bootstrap. Use static method getInstance instead');
		}
		self::$instance = $this;
		$this->resources = new \ArrayObject();
	}
	
	/**
	 * Static getter for bootstrap class instance
	 * 
	 * @return Bootstrap
	 */
	public static function getInstance() {
		if(!self::$instance) {
			return new static();
		}
		return self::$instance;
	}
	
	/**
	 * Resets the instance
	 * 
	 * @return bool
	 */
	public static function resetInstance() {
		self::$instance = null;
		return self::$instance == null;
	}
	
	/**
	 * Returns array with resource names, callbacks and if the resources has to be preloaded for the given object
	 * 
	 * @param object $object
	 * @return array
	 */
	protected function _extractResources($object) {
		$prefix_str = "(?:".$this->preload_prefix."|".$this->lazy_prefix.")";
		$method_pattern = sprintf(static::RESOURCE_METHOD_SEARCH, $prefix_str);
		$resources = array();
		$resource_names = preg_grep($method_pattern, get_class_methods($object));
		foreach($resource_names as $resource) {
			$resource_pattern = sprintf(static::RESOURCE_NAME_SEARCH, $prefix_str);
			$match = array();
			preg_match($resource_pattern, $resource, $match);
			$resource_name = strtolower($match[2]);
			$callable_name = $match[0];
			$preload = ($match[1] === $this->preload_prefix);
			$resources[] = array($resource_name, $callable_name, $preload);
		}
		return $resources;		
	}
	
	/**
	 * Registers a resource
	 * 
	 * @param string $resource_name
	 * @param mixed $callable
	 * @param bool $preload
	 * @return Bootstrap\Resource
	 * @throws Bootstrap\Resource\NameCollisionException
	 */
	public function registerResource($resource_name, $callable, $preload = false) {
		$resource_name = strtolower($resource_name);
		if($this->resources->offsetExists($resource_name)) {
			throw new Bootstrap\Resource\NameCollisionException("A resource with the name '$resource_name' is allready registered");
		}
		$resource = new Bootstrap\Resource($resource_name, $callable, $preload);
		$this->resources->offsetSet($resource_name, $resource);
		return $resource;
	}
	
	/**
	 * Registers resource methods from the given object.
	 * 
	 * @param object $object
	 * @return Bootstrap\InvalidArgumentException
	 */
	public function registerObject($object) {
		if(!is_object($object)) {
			throw new Bootstrap\InvalidArgumentException('Argument must be an object');
		}
		foreach($this->_extractResources($object) as $info) {
			list($resource_name, $callable_name, $preload) = $info;
			$this->registerResource($resource_name, array($object, $callable_name), $preload);
		}
		return $this;
	}
	
	/**
	 * Returns resources to preload which are not yet initialized
	 * 
	 * @return array
	 */
	protected function _getPreloadableResources() {
		$ret = array();
		foreach($this->getResources() as $resource) {
			if($resource->isPreload() && !$resource->isInitialized()) {
				$ret[] = $resource;
			}
		}
		return $ret;
	}
	
	/**
	 * Initializes all resources to be preloaded
	 * 
	 * @return \Phabs\Bootstrap
	 */
	public function init() {
		while($resources = $this->_getPreloadableResources()) {
			$resources[0]->init();
		}
		return $this;
	}
	
	/**
	 * Static convinience method for Bootstrap::init with optional registration of resource object
	 * 
	 * @see \Phabs\Bootstrap::init()
	 * @param object $object
	 * @return \Phabs\Bootstrap
	 */
	public static function bootstrap($object = null) {
		$instance = static::getInstance();
		if($object) {
			$instance->registerObject($object);
		}
		$instance->init();
		return $instance;
	}
	
	/**
	 * Returns list of currently registered resources
	 * 
	 * @return \ArrayObject
	 */
	public function getResources() {
		return $this->resources;
	}
	
	/**
	 * Returns resource object for the given resource name
	 * 
	 * @param string $resource_name
	 * @return Bootstrap\Resource
	 * @throws Bootstrap\InvalidArgumentException
	 * @throws Bootstrap\Resource\UnknownResourceException
	 */
	public static function getResource($resource_name) {
		if(!is_string($resource_name)) {
			throw new Bootstrap\InvalidArgumentException('Argument must be a string');
		}
		$resource_name = strtolower($resource_name);
		$resources = self::getInstance()->getResources();
		if(!$resources->offsetExists($resource_name)) {
			throw new Bootstrap\Resource\UnknownResourceException("Resurce $resource_name not found");
		}		
		return $resources->offsetGet($resource_name);		
	}
	
	/**
	 * Returns the value of the given resource name
	 * 
	 * @param string $resource_name
	 * @return mixed
	 */
	public static function getValue($resource_name) {
		return self::getResource($resource_name)->getValue();
	}
	
	/**
	 * Static onvinience method for use in resource methods to make sure that required resources are loaded and initialized
	 * 
	 * @param string|array $resource_names
	 */
	public static function requires($resource_names) {
		if(!is_array($resource_names)) {
			$resource_names = array($resource_names);
		}
		foreach($resource_names as $resource_name) {
			self::getResource($resource_name)->init();
		}
	}
}
