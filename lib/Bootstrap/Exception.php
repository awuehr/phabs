<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

namespace Phabs\Bootstrap;

require_once 'Resource/Exception.php';

class Exception extends \Exception {
	
}

class SingletonViolationException extends Exception {
	
}

class InvalidArgumentException extends Exception {
	
}