<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

namespace Phabs\Bootstrap;

require_once 'Exception.php';

/**
 * The class Resource describes a resource with information about loading strategy, initialization status and return value
 */
class Resource {
	/** @var string Name of the resource */
	protected $_name;
	
	/** @var mixed Callback which will be called by Resource::exec */
	protected $_callable;
	
	/** @var bool Determines if the resource is initialized */
	protected $_initialized = false;
	
	/** @var mixed Return value of the resource init method stored in Resource::$_callable */
	protected $_value = null;
	
	/** @var bool Determines if the resource should be initialized by the bootstrappers init method Bootstrapper::init */
	protected $_preload = false;
	
	/**
	 * Constructor for the resource descriptor class
	 * 
	 * @param string $resource_name
	 * @param mixed $callable
	 * @param bool $preload
	 * @throws Resource\InvalidArgumentException
	 */
	public function __construct($resource_name, $callable, $preload = false) {
		if(!is_callable($callable)) {
			throw new Resource\InvalidArgumentException('$callable needs to be a valid callback');
		}
		if(!is_string($resource_name)) {
			throw new Resource\InvalidArgumentException('$resource_name needs to be of type string');
		}
		if(!is_bool($preload)) {
			throw new Resource\InvalidArgumentException('$preload needs to be of type bool');
		}
		$this->_name = $resource_name;
		$this->_callable = $callable;
		$this->_preload = $preload;

	}
	
	/**
	 * Returns the name of the resource
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}
	
	/**
	 * Returns if the resource has to be initialized by Bootstrap::init()
	 * 
	 * @return bool
	 */
	public function isPreload() {
		return $this->_preload;
	}
	
	/**
	 * Returns if the resource has been initialized
	 * 
	 * @return bool
	 */
	public function isInitialized() {
		return $this->_initialized;
	}
	
	/**
	 * Returns the return value of the initialized resource
	 * 
	 * @return mixed
	 * @throws Resource\UninitializedResourceException
	 */
	public function getValue() {
		if(!$this->isInitialized()) {
			if($this->isPreload()) {
				throw new Resource\UninitializedResourceException('Cannot get value of uninitialized resource');
			} else {
				$this->init();
			}
		}
		return $this->_value;
	}
	
	/**
	 * Initializes the uninitialized resource
	 * 
	 * @see Resource::exec()
	 * @return mixed
	 */
	public function init() {
		if(!$this->isInitialized()) {
			$this->exec();
		}
		return $this->getValue();
	}
	
	/**
	 * Calls the resource init function/method
	 * 
	 * @return mixed
	 */
	protected function exec() {
		$this->_value = call_user_func_array($this->_callable, array());
		$this->_initialized = true;
		return $this->_value;
	}
}