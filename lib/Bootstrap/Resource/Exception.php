<?php

/**
 * Phabs - PHP Advanced Bootstrapper
 * 
 * Small and lightweight bootstrapping framework
 * 
 * @author Alexander Wühr <awuehr@gmail.com>
 * @copyright (c) 2013, Alexander Wühr
 * @license http://mit-license.org The MIT License (MIT)
 * @link https://bitbucket.org/awuehr/phabs Phabs on Bitbucket
 */

namespace Phabs\Bootstrap\Resource;

class Exception extends \Phabs\Bootstrap\Exception {
	
}

class NameCollisionException extends Exception {
	
}

class UnknownResourceException extends Exception {
	
}

class InvalidArgumentException extends \Phabs\Bootstrap\InvalidArgumentException {
	
}

class UninitializedResourceException extends Exception {
	
}